#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#include <Wire.h>
#include <SFE_MicroOLED.h>
//#include <SPI.h>
//#include <SD.h>

static const int RXPin = 4, TXPin = 3;
static const uint32_t GPSBaud = 9600;

#define PIN_RESET 9  // Connect RST to pin 9
#define PIN_DC    8  // Connect DC to pin 8
#define DC_JUMPER 0
MicroOLED oled(PIN_RESET, DC_JUMPER);

TinyGPSPlus gps;
SoftwareSerial ss(RXPin, TXPin);
unsigned long last = 0UL; // For stats that happen every 5 seconds

void setup() {
  Serial.begin(9600);
  ss.begin(GPSBaud);

  oled.begin();
  oled.clear(ALL);
  oled.display();
  smartDelay(1000);
  clearPage();
}

void clearPage() {
  oled.clear(PAGE);
  oled.setFontType(0);
  oled.setCursor(0, 0);
}

void loop() {
  smartDelay(0);
  clearPage();
  oled.print("V: ");
  oled.println(gps.speed.kmph());
  oled.print("H: ");
  oled.println(gps.altitude.meters());
  oled.print("S: ");
  oled.println(gps.satellites.value());
  oled.print("Lat: ");
  oled.println(gps.location.lat());
  oled.print("Lon: ");
  oled.println(gps.location.lng());
  oled.print("T ");
  oled.print(gps.time.hour()+2);
  oled.print(":");
  oled.print(gps.time.minute());
  oled.print(":");
  oled.println(gps.time.second());
  oled.display();
}

// This custom version of delay() ensures that the gps object
// is being "fed".
static void smartDelay(unsigned long ms)
{
  unsigned long start = millis();
  do 
  {
    while (ss.available())
      gps.encode(ss.read());
  } while (millis() - start < ms);
}

