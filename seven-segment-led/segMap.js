/*

This script spits out the array for displaying numbers on 7 segment display.

 aaaa       'a',00000010
f    b      'b',00000001
f    b      'c',00100000
f    b      'd',01000000
 gggg       'e',10000000
e    c      'f',00001000
e    c      'g',00010000
e    c hh   'h',00000100
 dddd  hh

*/

const segmentMap = new Map([
    ['a',0b00000010],
    ['b',0b00000001],
    ['c',0b00100000],
    ['d',0b01000000],
    ['e',0b10000000],
    ['f',0b00001000],
    ['g',0b00010000],
    ['h',0b00000100]
]);

const numbers = [
    ['a','b','c','d','e','f'],
    ['b','c'],
    ['a','b','g','e','d'],
    ['a','b','g','c','d'],
    ['f','b','g','c'],
    ['a','f','g','c','d'],
    ['a','f','g','c','d','e'],
    ['a','b','c'],
    ['a','b','c','d','e','f','g'],
    ['a','b','c','d','g','f']
];

const combineSegments = (segments, segmentMap) =>
    segments
        .map(segment => segmentMap.get(segment))
        .reduce((result, value) => result | value);

console.log("numbers[] = {" + numbers.map(number => combineSegments(number, segmentMap)).toString() + "};");
