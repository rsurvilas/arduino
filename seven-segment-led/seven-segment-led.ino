#include <vector>

using namespace std;

const int ONE = D1;
const int TEN = D2;
const int HUN = D3;
const int THO = D4;

const int DAT = D5;
const int CLK = D6;

const vector<int> displayPins = { ONE, TEN, HUN, THO };
const vector<int> dataPins = { DAT, CLK };
const vector<int> nums = {235,33,211,115,57,122,250,35,251,123};
vector<int> digits = {0,0,0,0};

void setup() {
  Serial.begin(9600);
  initPins(displayPins, LOW);
  initPins(dataPins, LOW);
  testRoutine();
  clear();
}

void initPins(vector<int> pins, const int state) {
  for(int pin : pins) {
    pinMode(pin, OUTPUT);
    digitalWrite(pin, state);
  }
}

void clear() {
  shiftOut(DAT, CLK, LSBFIRST, 0);
  for(int pin : displayPins)
    digitalWrite(pin, LOW);
}

void showNumber(int num, long delayMs) {
  digits = { num%10, num%100/10, num%1000/100, num/1000 };
  int len = getLength(num);

  unsigned long finish = millis() + delayMs;

  while (millis() < finish) {
    for(int i = 0; i < len; i++) {
      shiftOut(DAT, CLK, LSBFIRST, nums[digits[i]]);
      takeAbrake(1);
      digitalWrite(displayPins[i], HIGH);
      takeAbrake(5);
      clear();
    }
  }
}

int getLength(int num) {
  int len = 0;
  while (num > 0) {
    num /= 10;
    len++;
  }
  return len;
}

void takeAbrake(unsigned long ms) {
  unsigned long finish = millis()+ms;
  while (millis() < finish) {
    // do some useless shit here
  }
}

void testRoutine() {
  int segments[8] = {2,1,32,64,128,8,16,4};

  for (int digit : displayPins) {
    Serial.print(digit);
    Serial.print(": ");
    digitalWrite(digit, HIGH);
    
    for (int i = 0; i < 8; i++) {
      Serial.print(segments[i]);
      Serial.print(" ");
      shiftOut(DAT, CLK, LSBFIRST, segments[i]);
      takeAbrake(100);
    }

    yield(); // without his Watchdog timeout occurs (Soft WDT reset)

    Serial.println();
    digitalWrite(digit, LOW);
  }
}

int i = 0;
void loop() {
  showNumber(i++, 1);
  if (i > 9999) { i = 0; }
}
